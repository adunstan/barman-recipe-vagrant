Barman Recipe Summmary
======================

There are a number basic questions to answer in coming up with a recipe:

* Are you going to use streaming replication, or logfile shipping, or both?
* If both, are you going to use a replication slot (it's required if you're
  only using streaming)?
* Are you going to backup via rsync or pg_basebackup?
* Are you going to do base backups from a primary or a standby server?

barman setup
============

* `conninfo` is always required
* for streaming
    * `streaming_archiver = on` is required
	* if log shipping is not also being used, `slot_name` is required,
	  otherwise it's optional
	* `streaming_conninfo` should be set, but it defaults to `conninfo`
* for log shipping
    * `archiver = on` is required
* for streaming only
    * `archiver = off` should be set
* for backup via rsync
    * `backup_method = rsync` must be set
	* `ssh_command` must be set
* for backup via pg_basebackup
    * `backup_method = postgres` must be set
* for backup from a standby
    * `backup_method = rsync` must be set
	* `backup_options = concurrent_backup` must be set
	* `ssh_command` must point to the standby server
	* conninfo settings must point to the primary server
	(This might not always be true, but it is true for these recipes.)

server setup
============

* `wal_level = replica` is required
* for streaming and for backup via pg_basebackup
    * `max_wal_senders` must be set
	* if `slot_name` is set in barman `max_replication_slots` must be set
* for log shipping
    * `archive_mode = on` must be set
	* `archive_command` must be set to drop WAL files in barman's `incoming`
	  directory

Post-configuration
==================

Streaming setups require some extra steps after the configuration to work.

If you're using a replication slot, you need to run

````
barman receive-wal --create-slot servername
````

Then run

```
barman check servername > /dev/null
```

This will ensure that all the required directories are created.

If you're using streaming, you then need to run:

````
barman cron
barman switch-wal --force --archive servername
````

If you're running log shipping only you might not need to do anything, but
you also might need to do this, which in any case should be safe:

````
barman switch-wal --force --archive servername
````

Following these steps, check the setup with `barman check servername`.
It should all report OK.

Then you should be able to run

````
barman backup servername
````

This repository
===============

This repo contains a `vagrant` setup for CentOS7 that implements a
variety of barman recipes based on the above. It sets up three servers,
one for the various primary postgres instances, one for the standby instances,
and one for barman.

There is a startup script placed in the barman home directory that runs all
the setup and runs the first backup for each recipe. It can be called by
running

```
vagrant ssh barman -c "sudo su - barman -c 'sh -e startup'"
```

The generated recipe files and their descriptions are located in the `recipes`
folder, along with the startup script.
